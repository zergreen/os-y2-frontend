# Install Docker and Run on Ubuntu Linux (20.04 LTS)

ตัวโปรเจคนี้เราจะทำผ่าน OS : Linux Distro Ubuntu

dependency

* linux (ubuntu, debian)
 
![docker_os](https://miro.medium.com/max/1000/1*E8IgOSkMTpBRs0w0-Zsx2g.gif)

# Quick Test
ถ้าอยากจะ run แบบไวๆ ยังไม่ต้องถึงขั้นไปใช้ docker งั้นก็ใช้คำสั่งนี้
เพื่อรัน แบบ local ก่อน 
```bash
# clone and cd
git clone https://gitlab.com/zergreen/os-y2-frontend.git
cd os-y2-frontend

# install package
npm i

# run as server side rendering
npm run serve
```
ซึ่งถ้าเราทำส่วนนี้เสร็จแล้วก็ค่อยเอาไป deploy ยัดลง container ในภายหลัง
<b>อ่อละก็</b> file: mock-data.csv เป็น ไฟล์จำลอง data ที่จะแทรกในหน้าแรกนะ

# Content

- [ ] [ประวัติและความเป็นมา](#ประวัติและความเป็นมา)
- [ ] [ติดตั้ง Docker](#1-ติดตั้ง-docker)
- [ ] [Run Docker](#2-run-docker)
- [ ] [DockerImage](#3-dockerimage)
- [ ] [Run Script](#4-รันสคริปต์)
- [ ] [Docker container](#5-docker-container)
- [ ] [Docker Hub](#6-docker-hub)
- [ ] [รันโค้ดทาง ฝั่ง frontend && backend](#7-รันโค้ดทาง-ฝั่ง-frontend-backend)

# ประวัติและความเป็นมา
![virtualmachine](https://techglimpse.com/wp-content/uploads/2016/03/Container-vs-VMs.jpg)
Docker คือ Virtual machine engine (ตัวจำลอง os) ที่ Light weight กว่า Pure Virtual Machine อย่าง VMware , VirtualBox เพราะว่า มันไม่มีส่วน ของ Guest OS 
[Guest OS คือ OS ทั้งตัวพร้อมใช้ที่ติดตั้งมาให้แบบครบครน] ซึ่งมันก็ได้ตัดส่วน resourse ที่ไม่จำเป็นต่างๆ ออกไป อาทิ GUI, Desktop Manager, Network Manager ซึ่งมันก็แลกมาด้วยการที่เราจะต้อง config และ Download ตัวที่เราที่ต้องการจะใช้เองทั้งหมดตั้งแต่ต้น <br>
&nbsp; 


# 1. ติดตั้ง Docker 
  **1.1 ติดตั้ง Snap Store**
  คือ store ที่ออกแบบสำหรับ linux โดยเฉพาะ การติดตั้งจะง่ายและเร็วเพียงแค่รันผ่านคำสั่งเดียว มีตัวจัดการแอปที่เราดาวน์โหลดไว้ มองว่า เป็น type เดียวกับพวก *app store, play store และ windows store* เป็นต้น
  
  ```bash
  # update and install snap store
  $ sudo apt update
  $ sudo apt install snapd

  # ex. install app
  $ sudo snap install neofetch
  $ neofetch
  ```
  > ref : https://snapcraft.io/docs/installing-snap-on-ubuntu
  
  
  **1.2 ติดตั้ง Docker**
  
  ```bash
  $ sudo snap install docker
  ```
  
  **1.3 ให้สิทธิ์ Docker.socket** เพื่อเข้าถึงไฟล์ Docker
  
  ```bash
  $ sudo chmod 666 /var/run/docker.sock
  ```
  
---
 
# 2. Run Docker
  **2.1 Run alpine linux**

```bash
$ docker run --name web alpine
$ docker exec -it web sh
```
  
  **2.2 สอนใช้ linux alpine**
  
```bash
	# คำสั่งโหลดและติดตั้งแอพ [apk คล้ายกับ apt] ,[add คล้ายกับ install]
	$ apk add neofetch cmatrix ncdu neovim bat git
	
	# คำสั่ง บอกสเปคเครื่อง
	$ neofetch
	
	# คำสั่ง รันหน้า water fall 010101 เหมือนในหนัง hacker
	$ cmatrix
  ```

  **2.3 tldr docker (คำสั่งเบื้องต้น)**
  
	$ tldr docker
	
	docker
	Manage Docker containers and images.Some subcommands such as docker run have their own usage documentation.More information: https://docs.docker.com/engine/reference/commandline/cli/.
	
	 - List currently running docker containers:
	   docker ps
	
	 - List all docker containers (running and stopped):
	   docker ps -a
	
	 - Start a container from an image, with a custom name:
	   docker run --name {{container_name}} {{image}}
	
	 - Start or stop an existing container:
	   docker {{start|stop}} {{container_name}}
	
	 - Pull an image from a docker registry:
	   docker pull {{image}}
	
	 - Open a shell inside a running container:
	   docker exec -it {{container_name}} {{sh}}
	
	 - Remove a stopped container:
	   docker rm {{container_name}}
	
	 - Fetch and follow the logs of a container:
	   docker logs -f {{container_name}}
	   
*รวมคำสั่งจาก Docker จากเพจ BorntoDev*
![docker](https://scontent.fkdt1-1.fna.fbcdn.net/v/t1.6435-9/117770482_3349286511796285_5559642831373449148_n.jpg?_nc_cat=102&ccb=1-5&_nc_sid=730e14&_nc_eui2=AeFpbI3sX4scIYpID_A0yScP9mj-5hhBUTL2aP7mGEFRMhsa7Dh5uxLzSzGfYGkk9LqDS1Jxpxc5miNzVj0xN_uj&_nc_ohc=-Uy-j5oZrY0AX_jKfBX&_nc_ht=scontent.fkdt1-1.fna&oh=00_AT8mNAzZVYHdthy4gj1F6DcEVwllb3df8xD3KMsrd2B_tg&oe=6282301A)
> ref: https://www.facebook.com/borntodev


  
 **2.4 Tool**
  คือ เครื่องมือที่ใช้ manage docker container
  
  **ctop**
  ![ctop](https://mamchenkov.net/wordpress/wp-content/uploads/2020/02/Docker-Container-Monitoring.gif)
  
  **lazydocker**
  ![lazydocker](https://sectechno.com/wp-content/uploads/2020/07/lazydocker.gif)
  

# 3. Dockerimage

สอนเขียน

  **3.1 Dockerimage** ตย.ไฟล์ [Dockerfile](Dockerfile)
  
  **3.2 .dockerignore** ตย.ไฟล์ [.dockerignore](.dockerignore)
  
  **3.3 docker-compose.yml** ตย.ไฟล์ [.docker-compose.yml](.docker-compose.yml)
  
(ลิงค์ volume) 


# 4. Test Script
  **4.1 Dockerimage**
  
  **4.2 docker compose**


# 5. Docker container



# 6. Docker Hub
ก่อนอื่นให้เรา เข้าไปลงทะเบียน account ที่
> ref : https://hub.docker.com

เมื่อลงทะเบียนสร้างแอคเคาท์เสร็จแล้วให้กลับไปที่ terminal แล้วใช้คำสั่ง login เผื่อเตรียมตัว push local images ไปเป็น repository images บนคลาว์เว็บ

**6.1 docker login** 
คือ การ login docker hub

	docker login

**6.2 docker tag**
เป็นการ เปลี่ยนชื่อ docker image



	docker tag oldImageName your_userNameDocker/newImageName

**6.3 docker push**
คือ การอัพไฟล์บนเครื่อง ลงคลาว์ของ docker

	docker push your_userNameDocker/newImageName
	

# 7. รันโค้ดทาง ฝั่ง frontend && backend

***END***
---

# Contribute

Bukoree Dengmasa (backend) <br>
Phongsakorn Kawhaolai (fronted) <br>
Pisut Yimkuson (frontend) <br>
Pumipat Pisitratchanon (frontend) <br>
Worakon jantranon (frontend) <br>
Alongkorn Vanzoh (Project managedment, backend)

# Reference
https://youtu.be/8HdyPrlc_mk [สอน Docker เบื้องต้น] <br>
https://youtu.be/czonpQuOCFk [ลงมือทำจริง และอัดคลิป ส่งอาจารย์แล้ว] <br>
https://linuxtut.com/en/09384726105eee53bbb6/ [vue3 to Docker] <br>
https://gitlab.com/V-Develop/os-y2-frontend/-/tree/root/os-y2-frontend [MyFrontend--this Blog] <br>
https://gitlab.com/V-Develop/os-y2-backend/ [MyBackend]

