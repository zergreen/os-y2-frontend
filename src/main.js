// import "bootstrap/dist/js/bootstrap.js"
import VueGoogleCharts from "vue3-googl-chart";
import "bootstrap/dist/css/bootstrap.css"
import 'bootstrap'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

createApp(App).use(router, VueGoogleCharts).mount('#nupond')
