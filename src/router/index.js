import { createRouter, createWebHistory } from 'vue-router'

const routeOptions = [
  { path: "/",            name: "Home" },
  { path: "/contact",     name: "Contact" },
  { path: "/about",       name: "About" },
  { path: "/table",       name: "Table" },
  { path: "/dataTable",   name: "DataTable" },
  { path: "/chart",       name: "Chart" }
]

const routes = routeOptions.map(route => {
  return {
    ...route,
    component: () => import(`../views/${route.name}.vue`)
  }
})

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router